package com.faceme;

import com.faceme.Beans.RtcStats;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Component
public class Dispatcher {

    @Inject
    Persistence persistence;

    @Inject
    Producer producer;

    @PostConstruct
    public void start() {

        // get 1 item from queue and save in Redis

        RtcStats s = producer.poll();
        System.out.println(s);
        persistence.save(s);
    }
}
