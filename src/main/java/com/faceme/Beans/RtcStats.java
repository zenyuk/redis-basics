package com.faceme.Beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RtcStats implements Serializable{
    static final long serialVersionUID = 1L;

    private Long meetingSessionId;
    private Long lobbyPersonId;
    //    private MediaStats video;
    //    private MediaStats audio;
    private String timestamp;
    private Long rtt;
    private Long jitter;
    private Integer vcInstance;
    private String streamName;
}
