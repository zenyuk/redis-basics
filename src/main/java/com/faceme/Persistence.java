package com.faceme;

import com.faceme.Beans.RtcStats;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import javax.inject.Inject;

@Component
public class Persistence {

    @Inject
    RedisTemplate<String, RtcStats> redisTemplate;

    public void save(RtcStats stats) {
        String key = "rtc:vcgw:" + stats.getMeetingSessionId() + ":" + stats.getLobbyPersonId();

        System.out.println("key stored: " + key);

        ListOperations<String, RtcStats> ops = redisTemplate.opsForList();

        ops.leftPush(key, stats);
    }
}
