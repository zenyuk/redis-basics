package com.faceme;

import com.faceme.Beans.RtcStats;
import org.springframework.stereotype.Component;

@Component
public class Producer {

    public RtcStats poll() {
        RtcStats result = new RtcStats();
        result.setMeetingSessionId(2l);
        result.setLobbyPersonId(1l);
        result.setTimestamp("1234567893");
        result.setRtt(21l);
        result.setJitter(15l);
        result.setVcInstance(12);
        result.setStreamName("vfdcsteamhq");

        return result;
    }
}
