package com.faceme;

import com.faceme.Beans.RtcStats;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.inject.Inject;

@Component
@EnableScheduling
public class RedisReader {

    @Inject
    private RedisTemplate redisTemplate;

    @Scheduled(fixedDelay = 1000)
    public void read() {
        String key = "rtc:vcgw:2:1";
        ListOperations<String, RtcStats> ops = redisTemplate.opsForList();
        RtcStats result = ops.leftPop(key);

        System.out.println(result);
        if (result != null)
            System.out.println("from Redis: " + result.getStreamName());
    }
}
